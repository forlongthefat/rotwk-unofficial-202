# Modern UI example script
!include MUI.nsh
!include sections.nsh

Name "DodgeWars Replay Parser"
OutFile "dw.exe"
;InstallDir "C:\Replay Parser files"
SetCompressor lzma
InstallDirRegKey HKLM "Software\Electronic Arts\The Lord of the Rings, The Rise of the Witch-King" InstallPath

; Customization of the finish page (checkbox for desktop schortcut)
!define MUI_FINISHPAGE_SHOWREADME ""
!define MUI_FINISHPAGE_SHOWREADME_CHECKED
!define MUI_FINISHPAGE_SHOWREADME_TEXT "Create Desktop Shortcut"
!define MUI_FINISHPAGE_SHOWREADME_FUNCTION finishpageaction

;!define MUI_ICON "202icon.ico"
!define MUI_WELCOMEFINISHPAGE_BITMAP "welcomedw.bmp"

!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_LANGUAGE "English"

BrandingText "Rise of the Witch-King 2.02 -- www.gamereplays.org"


;;
; Main component
;;
Section
	;SectionIn RO ;Make it read-only
	SetOutPath "$INSTDIR\Replay Parser files"

	File /r "DodgeWars\*.*"

SectionEnd

;;
; Make a desktop shortcut if the user asked for it
;;
Function finishpageaction
	CreateShortcut "$desktop\Replay Parser.lnk" "$INSTDIR\UI.exe"
FunctionEnd



