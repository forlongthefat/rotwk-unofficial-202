# Modern UI example script
!include MUI.nsh
!include sections.nsh

Name "2.02 8.3.0 French Language Pack"
OutFile "202_v83_french_language.exe"
InstallDirRegKey HKLM "Software\Electronic Arts\Electronic Arts\The Lord of the Rings, The Rise of the Witch-king" InstallPath

SetCompressor /SOLID lzma

!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_LANGUAGE "English"

BrandingText "Rise of the Witch-King 2.02 -- www.gamereplays.org"

Var LANG

;;
; Main component
;;
Section
	DetailPrint "Determining language..."

	ReadRegStr $LANG HKLM "Software\Electronic Arts\Electronic Arts\The Lord of the Rings, The Rise of the Witch-king" Language
	DetailPrint "Your language is: $LANG"

	;Check if language is what we expect
	StrCmp $LANG "french" 0 noteq
		DetailPrint "Your game language is French."
		goto end

	noteq:
		DetailPrint "Your game language is not French."
		MessageBox MB_YESNO|MB_ICONEXCLAMATION "Your game does not seem to be installed in French. Are you sure you want to install the French language pack anyway?"  IDYES true IDNO false
		true:
			DetailPrint "Installing anyway..."
			Goto next
		false:
			DetailPrint "Aborting..."
			Abort
		next:

	end:

	;SectionIn RO ;Make it read-only
	SetOutPath $INSTDIR

	File /r "V83-French\*.*"
	Delete "$INSTDIR\lang\frenchstrings202_v7.0.0.big"
	Delete "$INSTDIR\launcher_releases\2.01 French.ini"
	Delete "$INSTDIR\launcher_releases\2.02 v7.0.0 French.ini"
SectionEnd



