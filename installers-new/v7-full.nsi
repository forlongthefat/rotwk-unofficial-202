# Modern UI example script
!include MUI.nsh
!include sections.nsh

Name "RotWK 2.02 version 7.0.0"
OutFile "202_v7_full.exe"
InstallDirRegKey HKLM "Software\Electronic Arts\Electronic Arts\The Lord of the Rings, The Rise of the Witch-king" InstallPath

;SetCompressor /SOLID lzma

; Customization of the license page (so it says readme rather than license)
!define MUI_PAGE_HEADER_TEXT "Read Me"
!define MUI_PAGE_HEADER_SUBTEXT "For new players"
!define MUI_LICENSEPAGE_TEXT_TOP "Please read the following information:"
!define MUI_LICENSEPAGE_TEXT_BOTTOM " . "
!define MUI_LICENSEPAGE_BUTTON "Next >"

; Customization of the finish page (checkbox for desktop schortcut)
!define MUI_FINISHPAGE_SHOWREADME ""
!define MUI_FINISHPAGE_SHOWREADME_NOTCHECKED
!define MUI_FINISHPAGE_SHOWREADME_TEXT "Create Desktop Shortcut"
!define MUI_FINISHPAGE_SHOWREADME_FUNCTION finishpageaction

!define MUI_ICON "202icon.ico"

!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE "license.rtf"
!insertmacro MUI_PAGE_COMPONENTS
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_LANGUAGE "English"

BrandingText "Rise of the Witch-King 2.02 -- www.gamereplays.org"


;;
; Main component
;;
Section
	;SectionIn RO ;Make it read-only
	SetOutPath $INSTDIR

	; delete all .disabled files, they can only interfere
	; this should be safe since only 2.02 makes these type of files
	FindFirst $0 $1 "$INSTDIR\*.disabled"
		loop:
			StrCmp $1 "" done
			Delete $1
			FindNext $0 $1
			Goto loop
		done:
	FindClose $0

	; remove the old lobbymusic mods
	FindFirst $0 $1 "$INSTDIR\___lobbymusic-*"
		loop2:
			StrCmp $1 "" done2
			Delete $1
			FindNext $0 $1
			Goto loop2
		done2:
	FindClose $0


	Delete "$INSTDIR\######unofficialpatch202_v4.0.0.big"  ;we ship this file in the installer as a .disabled. Last thing we want is this file screwing us over

	Delete "$INSTDIR\Unofficial202Launcher.exe"  ; delete old launcher
	Delete "$INSTDIR\Uninstall.exe"  ; delete old uninstaller

	; some misc ridisual files of previous 2.02 install
	Delete "$INSTDIR\_U202Changelog.txt"
	Delete "$INSTDIR\__launcherlocalization.txt"
	Delete "$INSTDIR\_launchercontrol.ini"
	Delete "$INSTDIR\_u202info.ini"


	; delete v7 beta related files
	FindFirst $0 $1 "$INSTDIR\!*"
		loop3:
			StrCmp $1 "" done3
			Delete $1
			FindNext $0 $1
			Goto loop3
		done3:
	FindClose $0

	; use the entire directory except the music-related files
	File /r "both/*"

	; Write the options.ini file for Win8 and Win10 users
	SetOutPath "$APPDATA\My The Lord of the Rings, The Rise of the Witch-king Files"
	SetOverwrite off
	File "Options.ini"
	SetOverwrite on

	; Reset back to regular install directory
	SetOutPath $INSTDIR

	writeUninstaller "$INSTDIR\uninstall02.exe"

	; Add the uninstaller to "add/remove programs" in Windows
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\GameReplaysRotWK" \
					"DisplayName" "Rise of the Witch-King 2.02"

	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\GameReplaysRotWK" \
					"Publisher" "RotWK 2.02 Team"

	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\GameReplaysRotWK" \
					"UninstallString" "$\"$INSTDIR\uninstall02.exe$\""
SectionEnd



;;
; Normal screen component
;;
Section "Widescreen" secws
	SetOutPath $INSTDIR
	File /r "v7ws\*"

SectionEnd

;;
; Wide screen component
;;
Section /o "4:3 monitor" secstd
	SetOutPath $INSTDIR
	File /r "v7std\*"

SectionEnd


;;
; Music component
;;
Section "Additional music" secmusic
	SetOutPath $INSTDIR
	delete "$INSTDIR\!vanilla202music.big"
	File /r "Music\*"

SectionEnd


Function .onInit
StrCpy $1 ${secws} ;The default
FunctionEnd

Function .onSelChange
!insertmacro StartRadioButtons $1
    !insertmacro RadioButton ${secws}
    !insertmacro RadioButton ${secstd}
!insertmacro EndRadioButtons
FunctionEnd

;;
; Make a desktop shortcut if the user asked for it
;;
Function finishpageaction
	CreateShortcut "$desktop\2.02 Switcher.lnk" "$INSTDIR\new_launcher.exe"
FunctionEnd


;;
; Helper function that removes the content of a directory
;;
!macro RemoveFilesAndSubDirs DIRECTORY
  !define Index_RemoveFilesAndSubDirs 'RemoveFilesAndSubDirs_{__LINE__}'

  Push $0
  Push $1
  Push $2

  StrCpy $2 "${DIRECTORY}"
  FindFirst $0 $1 "$2*.*"
${Index_RemoveFilesAndSubDirs}-loop:
  StrCmp $1 "" ${Index_RemoveFilesAndSubDirs}-done
  StrCmp $1 "." ${Index_RemoveFilesAndSubDirs}-next
  StrCmp $1 ".." ${Index_RemoveFilesAndSubDirs}-next
  IfFileExists "$2$1\*.*" ${Index_RemoveFilesAndSubDirs}-directory
  ; file
  Delete "$2$1"
  goto ${Index_RemoveFilesAndSubDirs}-next
${Index_RemoveFilesAndSubDirs}-directory:
  ; directory
  RMDir /r "$2$1"
${Index_RemoveFilesAndSubDirs}-next:
  FindNext $0 $1
  Goto ${Index_RemoveFilesAndSubDirs}-loop
${Index_RemoveFilesAndSubDirs}-done:

  FindClose $0

  Pop $2
  Pop $1
  Pop $0
  !undef Index_RemoveFilesAndSubDirs
!macroend



;;
; This spawns the confirmation window when uninstalling
;;
function un.onInit
	; Verify the uninstaller - last chance to back out
	MessageBox MB_OKCANCEL "Permanantly remove RotWK 2.02 version 7.0.0?" IDOK next
		Abort
	next:

functionEnd

;;
; Uninstaller section
;;
section "uninstall"
	SetOutPath "$INSTDIR"

	; first, we delete the dat files, whichever version tey may be
	delete "$INSTDIR\game.dat"
	delete "$INSTDIR\game.other"
	delete "$INSTDIR\asset.dat"
	delete "$INSTDIR\asset.other"

	; then, we manually install the old asset and game dats
	File "both\game.other"
	File "both\asset.other"

	; And of course rename them to the proper name
	CopyFiles "$INSTDIR\game.other" "$INSTDIR\game.dat"
	delete "$INSTDIR\game.other"
	CopyFiles "$INSTDIR\asset.other" "$INSTDIR\asset.dat"
	delete "$INSTDIR\asset.other"

	; Remove Start Menu launcher
	delete "$desktop\2.02 Switcher.lnk"

	; delete all .disabled files, they can only interfere
	; this should be safe since only 2.02 makes these type of files
	FindFirst $0 $1 "$INSTDIR\*.disabled"
		loop:
			StrCmp $1 "" done
			Delete $1
			FindNext $0 $1
			Goto loop
		done:
	FindClose $0


	; delete all 202 related files
	FindFirst $0 $1 "$INSTDIR\*202*"
		loop3:
			StrCmp $1 "" done3
			Delete $1
			FindNext $0 $1
			Goto loop3
		done3:
	FindClose $0


	; delete all lang files in the ugliest way possible
	SetShellVarContext current

	${For} $R1 1 20 ; make sure the rightmost number exceeds the amount of lang files
		Delete "$INSTDIR\lang\*202*"
	${Next}

	SetShellVarContext all


	!insertmacro RemoveFilesAndSubDirs "$INSTDIR\launcher_releases\"


	; Delete launcher and related files
	delete "$INSTDIR\new_launcher.exe"
	delete "$INSTDIR\bg.png"
	delete "$INSTDIR\new_launcher_source.zip"
	rmDir "$INSTDIR\launcher_releases"

	; Delete the uninstaller key for "add/remove programs"
	DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\GameReplaysRotWK"

	; Always delete uninstaller as the last action
	delete "$INSTDIR\uninstall02.exe"
sectionEnd

;;
; Descriptions that show up on the components page
;;
!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
	!insertmacro MUI_DESCRIPTION_TEXT ${secmusic} "This component contains the improved music pack which offers a more varied, larger selection of LoTR music."
	!insertmacro MUI_DESCRIPTION_TEXT ${secstd} "This component contains the patch for old monitors (4:3 resolution)."
	!insertmacro MUI_DESCRIPTION_TEXT ${secws} "This component contains the patch for widescreen monitors (16:9 resolution)."


!insertmacro MUI_FUNCTION_DESCRIPTION_END

