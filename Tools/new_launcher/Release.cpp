/*
 * Release.cpp
 *
 * Copyright 2017 Bram Bosch <bagbosch@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#define DISABLED_EXTENSION ".disabled"

#include "Release.h"

// For file handling
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QDebug>

Release::Release(QString newName)
{
	name = newName;
}

Release::Release()
{
	name = QString("");
}

QString Release::getName()
{
	return name;
}

QString Release::applyRelease()
{
	QString errorMsg = "";

	foreach (Release toenable, triggeredReleases)
	{
		QString response = toenable.applyRelease();

		if(response.compare("") != 0)
			errorMsg = response;
	}

	foreach (QString toenable, enableFiles)
	{
		bool enabledproblematicFile = enableFile(toenable);

		if(enabledproblematicFile == false ) 
			errorMsg = QString("Could not enable file ") + toenable;
	}

	foreach (QString todisable, disableFiles)
	{
		bool disabledproblematicFile = disableFile(todisable);

		if(disabledproblematicFile == false ) 
			errorMsg = QString("Could not disable file ") + todisable + "\n\nTry running this launcher with administration rights.";
	}

	foreach (SwitchFile toswitch, switchFiles)
	{
		QString message = toswitch.performSwitch();

		if(message.compare("") != 0 ) 
			errorMsg = QString("Could not switch file ") + toswitch.getFileName() + "\nbecause: " + message + "\n\nMost likely an anti-virus program has touched some of the 2.02 files, making the system incomlete. Try releasing the above mentioned file from quarantine. \nIf that does not work, disable your anti-virus software and reinstall 2.02.";
	}

	return errorMsg;
}

void Release::addSwitchFile(SwitchFile toAdd)
{
	switchFiles.push_front(toAdd);
}

SwitchFile Release::getSwitchFile(int fileID)
{
	if (fileID >= switchFiles.size()) {
		return SwitchFile();
	}
	return switchFiles.at(fileID);
}



void Release::addEnableFile(QString toAdd)
{
	enableFiles.push_front(toAdd);
}

QString Release::getEnableFile(int fileID)
{
	if (fileID >= enableFiles.size()) {
		return "Error:NoFile";
	}
	return enableFiles.at(fileID);
}


void Release::addDisableFile(QString toAdd)
{
	disableFiles.push_front(toAdd);
}

void Release::addEnableFileChecksum(QString toAdd)
{
	enableFileChecksums.push_front(toAdd);
}

void Release::addTriggeredRelease(Release toAdd)
{
	triggeredReleases.push_front(toAdd);
}

bool Release::disableFile(QString fileName)
{
	QFile f(fileName);
	QFileInfo finfo(f);

	// This is a string representing the absolute filename with its extension replaced by ".disabled"
	QString asDisbled = finfo.absolutePath().append(QString("/").append(finfo.completeBaseName().append(DISABLED_EXTENSION)));

	QFile disabledFile(asDisbled);

	// File is already disabled
	if (disabledFile.exists()) {
		if (f.exists()) {
			if ( fileSHA1(f.fileName()).compare(fileSHA1(disabledFile.fileName())) == 0) {
				f.remove();
				return true;
			}
			else
				return false;
		}
		else
			return true;
	}
	
	// If the file is enabled, let's disable it
	if (f.exists())
	{
		bool ok = f.rename(asDisbled);

		if(ok)
			return true;
	}

	return false;
}


bool Release::enableFile(QString fileName)
{
	QFile f(fileName);
	QFileInfo finfo(f);

	// This is a string representing the filename with its extension replaced by ".disabled"
	QString asDisbled = finfo.absolutePath().append(QString("/").append(finfo.completeBaseName().append(DISABLED_EXTENSION)));

	QFile disabledFile(asDisbled);

	// If the file exists (and is therefore already 'activated'), we don't need to do anything
	if (f.exists())
		return true;

	// If the file is disabled, let's rename it
	if (disabledFile.exists())
	{
		QFileInfo finfoDisabled(disabledFile);
		QString newName = finfoDisabled.absolutePath().append(QString("/").append(finfo.fileName()));
		
		bool ok = disabledFile.rename(newName);

		if(ok)
			return true;
	}

	return false;
}

bool Release::integrityCheckFile(int fileID)
{
	if (fileID >= enableFiles.size()) {
		qWarning("You're trying to verify the integrity of a file that's not part of this release.");
		return false;
	}
	if (fileID >= enableFileChecksums.size()) {
		qWarning("Can't integrity verify. Checksums incomplete for this release.");
		return false;
	}

	QString fileName = enableFiles.at(fileID);
	QString supposedChecksum = enableFileChecksums.at(fileID);

	QString computedChecksum = fileSHA1(fileName);


	if (computedChecksum.compare(supposedChecksum))
		return false;


	return true;
}

QString Release::integrityCheckRelease()
{
	for( int i=0; i<enableFiles.size(); i++ ) {
		if(integrityCheckFile(i) == false) {
			return enableFiles.at(i) + " is missing or corrupt...";;
		}
	}
	for( int i=0; i<switchFiles.size(); i++ ) {
		SwitchFile curr = switchFiles.at(i);
		if(curr.isAlreadyActive() == false) {
			SwitchFile returnval = switchFiles.at(i);
			return returnval.getFileName() + " was not switched to the correct version properly...";
		}
	}

	QDir dir(".");

	if (!dir.exists()) {
		qWarning("The directory does not exist");
		return "All files lel. The directory does not even exist... WHAT?!";
	}
	else
	{
		dir.setFilter(QDir::Files | QDir::AllDirs);
		dir.setSorting(QDir::Size | QDir::Reversed);

		QFileInfoList list = dir.entryInfoList();

		foreach (QFileInfo finfo, list)
		{
			foreach (QString supposedlyDisabled, disableFiles)
			{
			if( supposedlyDisabled.compare(finfo.fileName()) == 0 )   //if we find a file that should be disabled, the release is not valid
				return supposedlyDisabled + " should be disabled, but it's still enabled.";
			}
		}
	}

	return "";
}

/**
 * Calculates an sha1 checksum from a file.
 *
 * Will return the string "Error" if something went wrong
 *
 * Mapping:  QString -> QString
 */
QString Release::fileSHA1(QString file_name)
{
	const char * output = "Error";

	QFile f(file_name);
	QCryptographicHash hash(QCryptographicHash::Sha1);

	if (f.open(QFile::ReadOnly))
	{
		hash.addData(f.readAll());

		output = hash.result().toHex();
	}

	return QString(output);
}


