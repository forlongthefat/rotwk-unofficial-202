/*
 * ReplayTool.h
 *
 * Copyright 2017 Bram Bosch <bagbosch@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#pragma once

#include <QWidget>
#include <QApplication>
#include <QPushButton>
#include <QLabel>

#include <QTextStream>
#include <QFile>
#include <QDir>
#include <QLineEdit>
#include <QDesktopWidget>
#include <QMainWindow>
#include <QApplication>
#include <QGridLayout>
#include <QMessageBox>
#include <QSpacerItem>
#include <QMenu>
#include <QMenuBar>
#include <QPixmap>
#include <QPalette>
#include <QFont>
#include <QSignalMapper>
#include <QDesktopServices>
#include <QGroupBox>
#include <QRadioButton>
#include <QVBoxLayout>

#include <QObject>
#include <QEventLoop>
#include <QFileSystemWatcher>
#include <QDirIterator>
#include <QSettings>

#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>


class ReplayTool : public QWidget
{
	Q_OBJECT

	public:
		ReplayTool(QWidget *parent = 0);
		void ShowLoader(QString loaderText);

	public slots:
		void SetupWindow();

	private slots:
		void LogIn();
		void FinalizeLogin(QNetworkReply * reply);
		void FinalizedLoginUItrigger(QString email);
		void LogOut();
		void GetNicknames();
		void SelectFile();
		void SelectFileForReport();
		void DetectedModification(const QString& str);
		void DetectedDirModification(const QString& str);
		void ExecuteAutoDetectReport();
		void ExecuteReport(QFileInfo reportFile);
		void FinalizeExecuteReport(QNetworkReply * reply);
		void FinalizeNicknameRequest(QNetworkReply * reply);
		void RescanAllTrackingFiles(const QString& str);
		void ToggleAdvanced();

	private:
		QGroupBox *SpawnAccountBox();
		QGroupBox *SpawnConfigurationBox();
		QGroupBox *SpawnReportingBox();
		QGroupBox *SpawnAdvancedSettingsBox();
		QFileInfo FindMostRecentReplayFilePath(QString replayFolder);
		void SetNewReplayFolder(QString selectedFolder);
		void UpdateReplayInfo(QFileInfo replayFinfo);
		bool IsAlreadyReportedHash(QString sha1sum, QSettings *settings);
		QString HashFile(QString path);

		QMessageBox *loader;

		QLineEdit *EmailBox;
		QLineEdit *PasswordBox;
		QPushButton *loginButton;
		QPushButton *logoutButton;
		QPushButton *nicknamesButton;
		QLabel *StatusLabel;

		QLineEdit *ReplayDirBox;
		QPushButton *SelectReplayDirButton;
		QFileSystemWatcher *ReplayDirWatcher;

		QLabel *MostRecentReplayLabel;
		QPushButton *ReportButon;
		QPushButton *ReportCustomFileButon;

		QString baseUrl;
		QGroupBox *advancedOptionsBox;
};
