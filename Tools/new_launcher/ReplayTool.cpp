/*
 * ReplayTool.cpp
 *
 * Copyright 2017 Bram Bosch <bagbosch@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include "ReplayTool.h"
#include "UserDirFind.h"
#include "qt-json/json.h"
#include "Release.h"

#include <QGridLayout>
#include <QFileDialog>
#include <QProcess>
#include <QDebug>
#include <QDataStream>
#include <QFileDialog>
#include <QDateTime>
#include <QHttpMultiPart>
#include <QCheckBox>


const QString FrameStyleSheet(
		"QGroupBox{															\
			border:1px solid gray;											\
			border-radius:5px;												\
			margin-top: 1ex;												\
		} 																	\
		QGroupBox::title{													\
			subcontrol-origin: margin;subcontrol-position:top center;														\
			padding:0 0;													\
		}");

const int ButtonWidth = 120;
const QString checkmark = QString::fromUtf8("\u2713");

ReplayTool::ReplayTool(QWidget *parent) : QWidget(parent)
{
	loader = new QMessageBox();
	loader->hide();
	ReplayDirWatcher = new QFileSystemWatcher(this);

	baseUrl = "https://api.rotwk-league.net/api/";

	QSettings s("./replay-parser-settings.dat", QSettings::IniFormat);
	QString url = s.value("url").toString();

	if(url.compare("") != 0)
		baseUrl = url;
}

/**
 * This is only executed once the window is booted up by the user.
 * Otherwise, you'd get stuff like logging in already at launcher startup.
 */
void ReplayTool::SetupWindow()
{
	QGridLayout *grid = new QGridLayout(this);

	advancedOptionsBox = SpawnAdvancedSettingsBox();

	grid->addWidget(SpawnAccountBox(), 0, 0);
	grid->addWidget(SpawnReportingBox(), 2, 0);
	grid->addWidget(SpawnConfigurationBox(), 1, 0);
	grid->addWidget(advancedOptionsBox, 4, 0);

	advancedOptionsBox->hide();
	
	QCheckBox *unlockAdvancedOptions = new QCheckBox;
	unlockAdvancedOptions->setText("Show advanced options");
	
	connect(unlockAdvancedOptions, SIGNAL(clicked()), this, SLOT(ToggleAdvanced()));

	grid->addWidget(unlockAdvancedOptions, 3, 0);
	
	grid->setVerticalSpacing(20);
	setLayout(grid);

	loader->hide();
}

/**
 * Contains all widgets in the "Account" category.
 */
QGroupBox * ReplayTool::SpawnAccountBox()
{
	QLabel *EmailLabel = new QLabel("Login:", this);
	QLabel *PasswordLabel = new QLabel("Password:", this);
	StatusLabel = new QLabel("Status: logged out.", this);

	EmailBox = new QLineEdit("", this);
	PasswordBox = new QLineEdit("", this);
	PasswordBox->setEchoMode(QLineEdit::Password);
	EmailBox->setPlaceholderText("User@domain.com");
	PasswordBox->setPlaceholderText("********");

	loginButton = new QPushButton("Log in", this);
	loginButton->setShortcut(QKeySequence(Qt::Key_Return));
	
	logoutButton = new QPushButton("Log out", this);
	nicknamesButton = new QPushButton("My nicknames", this);

	QGroupBox *groupBox1 = new QGroupBox("Account");
	groupBox1->setStyleSheet(FrameStyleSheet);

	QGridLayout *box1Layout = new QGridLayout;
    groupBox1->setLayout(box1Layout);

	box1Layout->addWidget(EmailLabel, 0, 0);
	box1Layout->addWidget(PasswordLabel, 1, 0);

	box1Layout->addWidget(EmailBox, 0, 1);
	box1Layout->addWidget(PasswordBox, 1, 1);

	box1Layout->addWidget(StatusLabel, 0, 2, 2, 1);
	StatusLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);

	box1Layout->addWidget(loginButton, 2, 0);
	box1Layout->addWidget(logoutButton, 3, 2, Qt::AlignRight);
	box1Layout->addWidget(nicknamesButton, 2, 2, Qt::AlignRight);
	
	logoutButton->hide();
	nicknamesButton->hide();

	EmailBox->setMinimumWidth(200);
	PasswordBox->setMinimumWidth(200);
	StatusLabel->setMinimumWidth(175);
	loginButton->setMinimumWidth(ButtonWidth);
	logoutButton->setMinimumWidth(ButtonWidth);
	nicknamesButton->setMinimumWidth(ButtonWidth);

	QSettings s("./replay-parser.ini", QSettings::IniFormat);
	QString confEmail = s.value("email").toString();
	QString confPwd = s.value("token").toString();

	if(confEmail.compare("") != 0 && confPwd.compare("") != 0) {
		qWarning("U already have login pl0x");
		EmailBox->setText(confEmail);
		PasswordBox->setText("dummy");
		FinalizedLoginUItrigger(confEmail);
		//No longer need to log in as we have a token.
		//LogIn();
	}

	connect(loginButton, SIGNAL(clicked()), this, SLOT(LogIn()));
	connect(logoutButton, SIGNAL(clicked()), this, SLOT(LogOut()));
	connect(nicknamesButton, SIGNAL(clicked()), this, SLOT(GetNicknames()));

	return groupBox1;
}


/**
 * Contains all widgets in the "Configuration" category.
 */
QGroupBox * ReplayTool::SpawnConfigurationBox()
{
	QString dirs[2];
	UserDirFind::FindRotwkUserDirs(dirs);
	QString replayFolder(dirs[0] + "/Replays");
	QString replayFolderB(dirs[1] + "/Replays");
	QFile optsB(replayFolderB);

	if(optsB.exists())
		replayFolder = replayFolderB;

	QSettings s("./replay-parser.ini", QSettings::IniFormat);
	QString overridePath = s.value("path").toString();

	if(overridePath.compare("") != 0)
		replayFolder = overridePath;

	QLabel *ReplayPathLabel = new QLabel("Replay folder:", this);

	ReplayDirBox = new QLineEdit("", this);
	ReplayDirBox->setMinimumWidth(600);
	ReplayDirBox->setReadOnly(true);

	QDir replayFolderDir(replayFolder);

	// It doesn't exist!?
	if(!(replayFolderDir.exists()))
	{
		ReplayDirBox->setPlaceholderText("???");
	}
	else {
		ReplayDirBox->setText(replayFolder);

		
		replayFolderDir.cd("..");

		SetNewReplayFolder(replayFolderDir.absolutePath());
	}

	UpdateReplayInfo(FindMostRecentReplayFilePath(replayFolder));

	SelectReplayDirButton = new QPushButton("Select", this);

	QGroupBox *groupBox1 = new QGroupBox("Configuration");
	groupBox1->setStyleSheet(FrameStyleSheet);
	QGridLayout *box1Layout = new QGridLayout;
    groupBox1->setLayout(box1Layout);

	box1Layout->addWidget(ReplayPathLabel, 0, 0);
	box1Layout->addWidget(ReplayDirBox, 0, 1);
	box1Layout->addWidget(SelectReplayDirButton, 0, 2);
	SelectReplayDirButton->setMinimumWidth(ButtonWidth);

	connect(SelectReplayDirButton, SIGNAL(clicked()), this, SLOT(SelectFile()));

	return groupBox1;
}

/**
 * Contains all widgets in the "Reporting" category.
 */
QGroupBox * ReplayTool::SpawnReportingBox()
{
	QLabel *MostRecentReplayDescriptorLabel = new QLabel("Replay name: \nCreated on:\n ", this);
	MostRecentReplayLabel = new QLabel("<<placeholder>>", this);

	QGroupBox *groupBox1 = new QGroupBox("Reporting");
	groupBox1->setStyleSheet(FrameStyleSheet);
	QGridLayout *box1Layout = new QGridLayout;
    groupBox1->setLayout(box1Layout);
	
	ReportButon = new QPushButton("Report loss", this);

	box1Layout->addWidget(MostRecentReplayDescriptorLabel, 0, 0);
	box1Layout->addWidget(MostRecentReplayLabel, 0, 1, Qt::AlignLeft);
	box1Layout->addWidget(ReportButon, 1, 1, Qt::AlignRight);

	MostRecentReplayDescriptorLabel->setMaximumWidth(120);
	ReportButon->setMinimumWidth(ButtonWidth);

	connect(ReportButon, SIGNAL(clicked()), this, SLOT(ExecuteAutoDetectReport()));

	return groupBox1;
}

QGroupBox * ReplayTool::SpawnAdvancedSettingsBox()
{
	QGroupBox *groupBox1 = new QGroupBox("Advanced");
	groupBox1->setStyleSheet(FrameStyleSheet);

	QGridLayout *box1Layout = new QGridLayout;
    groupBox1->setLayout(box1Layout);

	ReportCustomFileButon = new QPushButton("Custom file report", this);

	box1Layout->addWidget(ReportCustomFileButon, 0, 0, Qt::AlignLeft);

	ReportCustomFileButon->setMinimumWidth(ButtonWidth);
	ReportCustomFileButon->setMaximumWidth(ButtonWidth);

	connect(ReportCustomFileButon, SIGNAL(clicked()), this, SLOT(SelectFileForReport()));

	return groupBox1;
}

void ReplayTool::ShowLoader(QString loaderText)
{
	loader->hide();

	loader = new QMessageBox();
	loader->setText(loaderText);
	loader->setWindowTitle("League report tool");
	loader->setStandardButtons(0);
	loader->setVisible(true);
	loader->setMinimumWidth(300);
	loader->show();
}

void ReplayTool::GetNicknames()
{
	QNetworkRequest *nicknameRequestDW = new QNetworkRequest(
		QUrl(baseUrl + "account/login-nicknames?login=" + EmailBox->text()));

	nicknameRequestDW->setRawHeader("ClientName", "console");

	QNetworkAccessManager *manager = new QNetworkAccessManager(this);
	connect(manager, SIGNAL(finished(QNetworkReply *)), this, SLOT(FinalizeNicknameRequest(QNetworkReply *)));

	manager->get(*nicknameRequestDW);

	ShowLoader("Connecting to server...");
}

void ReplayTool::FinalizeNicknameRequest(QNetworkReply * reply)
{	
	loader->hide();

	QString responseString(reply->readAll());
	qDebug() << "Response: " << responseString << "\n";

	if (reply->error() != 0)
	{
		QMessageBox messageBox;
		switch(reply->error()) {
			case 99: messageBox.critical(0, "Report loss", "Can't establish connection... Is your internet connection working?"); break;
			case 299: messageBox.critical(0, "Report loss", QString("Nickname request failed... \n\n" + responseString + "\n\n" + reply->rawHeader("Date")) ); break;
			default: messageBox.critical(0, "Report loss", QString("Problem while retrieving nicknames... \n") + reply->errorString()); break;
		}
		messageBox.setFixedSize(500,200);
		return;
	}

	bool ok;
	QtJson::JsonArray result = QtJson::parse(responseString, ok).toList();

	
	QString nicknames("");
	foreach(QVariant nickname, result) {
		nicknames += QString("- ") + nickname.toString() + "\n";
	}

	nicknames.chop(1);

	qDebug() << "Response: " << nicknames << "\n";

	QMessageBox messageBox;
	messageBox.information(0, "Nicknames", QString("Your registered nicknames are: \n" + nicknames));
	messageBox.setFixedSize(500,200);
}

void ReplayTool::LogIn()
{
	QNetworkRequest *tokenRequestDW = new QNetworkRequest(
		QUrl(baseUrl + "account/gettoken"));

	tokenRequestDW->setRawHeader("Content-Type", "application/json");
	tokenRequestDW->setRawHeader("ClientName", "console");

	QtJson::JsonObject userinfo;
	userinfo["Email"] = EmailBox->text();
	userinfo["Password"] = PasswordBox->text();

	QNetworkAccessManager *manager = new QNetworkAccessManager(this);
	connect(manager, SIGNAL(finished(QNetworkReply *)), this, SLOT(FinalizeLogin(QNetworkReply *)));

	manager->setProperty("Email", EmailBox->text());
	manager->setProperty("Password", PasswordBox->text());
	manager->post(*tokenRequestDW, QtJson::serialize(userinfo));

	ShowLoader("Connecting to server...");
}

void ReplayTool::FinalizeLogin(QNetworkReply * reply)
{	
	loader->hide();

	QString responseString(reply->readAll());
	qDebug() << "Response: " << responseString << "\n";

	if (reply->error() != 0)
	{
		qDebug() << "Errorcode = " << reply->error()  << "\n";
		QMessageBox messageBox;
		switch(reply->error()) {
			case 99: messageBox.critical(0, "Log in", "Can't establish connection... Is your internet connection working?"); break;
			case 299: messageBox.critical(0, "Log in", QString("Log in refused. \n\n") + responseString + "\n\n" + reply->rawHeader("Date")); break;
			default: messageBox.critical(0, "Log in", QString("Problem while logging in... \n") + reply->errorString()); break;
		}
		messageBox.setFixedSize(500,200);
		return;
	}


	QString email = sender()->property("Email").toString();
	QString pwd = sender()->property("Password").toString();

	QSettings s("./replay-parser.ini", QSettings::IniFormat);
	s.setValue("email", email);
	s.setValue("token", responseString);
	s.sync();

	FinalizedLoginUItrigger(email);
}

void ReplayTool::FinalizedLoginUItrigger(QString email)
{
	QMessageBox messageBox;
	messageBox.information(0, "Log in", QString("Successfully logged in as: " + email));
	messageBox.setFixedSize(500,200);

	EmailBox->setDisabled(1);
	PasswordBox->setDisabled(1);
	
	StatusLabel->setText(QString("Status: logged in.  ") + checkmark);

	loginButton->hide();
	logoutButton->show();
	nicknamesButton->show();

	return;
}

void ReplayTool::LogOut()
{
	QSettings s("./replay-parser.ini", QSettings::IniFormat);
	s.remove("email");
	s.remove("pwd");
	s.sync();

	EmailBox->setDisabled(0);
	PasswordBox->setDisabled(0);
	loginButton->show();
	logoutButton->hide();
	nicknamesButton->hide();

	StatusLabel->setText("Status: logged out.");
}

void ReplayTool::SelectFile()
{
	QString selectedFolder = QFileDialog::getExistingDirectory(
		this,
		tr("Select Replay Directory"),
		QDir(UserDirFind::AppdataDirFind()).absolutePath(),
		QFileDialog::ShowDirsOnly);

	SetNewReplayFolder(selectedFolder);
}

void ReplayTool::SelectFileForReport()
{
	if (loginButton->isVisible())
	{
		QMessageBox messageBox;
		messageBox.critical(0, "Report loss", "Please log in before reporting.");
		messageBox.setFixedSize(500,200);
		return;
	}

	QString selectedFolder = QFileDialog::getOpenFileName(
		this, 
		tr("Open File"),
		QDir(UserDirFind::AppdataDirFind()).absolutePath(),
		tr("Replays (*.bfme2replay)"));

	if(selectedFolder.compare("") == 0)
		return;

	QFileInfo reportFile(selectedFolder);
	
	ExecuteReport(reportFile);
}

void ReplayTool::DetectedModification(const QString& str)
{
	QFileInfo strInfo(str);

	QFileInfo replayFinfo =  FindMostRecentReplayFilePath(strInfo.dir().absolutePath());
	qDebug() << "Most recently changed file: " << replayFinfo.fileName() <<"\n";

	UpdateReplayInfo(replayFinfo);
}

void ReplayTool::DetectedDirModification(const QString& str)
{
	RescanAllTrackingFiles(str);
	DetectedModification((str + "/leldummy.txt"));
}

void ReplayTool::RescanAllTrackingFiles(const QString& str)
{
	QDir dir(str);
	dir.setFilter(QDir::Files);

	QFileInfoList list = dir.entryInfoList();

	//Watch all files in the directory for modifications
	foreach (QFileInfo finfo, list){
		if(finfo.suffix().compare("bfme2replay", Qt::CaseInsensitive) == 0){
			ReplayDirWatcher->addPath(finfo.filePath());
			qDebug() << finfo.filePath();
		}
	}

	//Watch the most recently modified file for modifications
	connect(ReplayDirWatcher, SIGNAL(fileChanged(QString)), this, SLOT(DetectedModification(QString)));
}

void ReplayTool::ExecuteAutoDetectReport()
{
	if (loginButton->isVisible())
	{
		QMessageBox messageBox;
		messageBox.critical(0, "Report loss", "Please log in before reporting.");
		messageBox.setFixedSize(500,200);
		return;
	}

	if(ReplayDirWatcher->directories().isEmpty())
	{
		QMessageBox messageBox;
		messageBox.critical(0, "Report loss", "Please select a valid replay directory before reporting.");
		messageBox.setFixedSize(500,200);
		return;
	}

	QFileInfo reportFile = FindMostRecentReplayFilePath(ReplayDirWatcher->directories().last());
	if(!reportFile.exists())
	{
		QMessageBox messageBox;
		messageBox.critical(0, "Report loss", "Please select a valid replay directory before reporting.");
		messageBox.setFixedSize(500,200);
		return;
	}

	ExecuteReport(reportFile);
}

void ReplayTool::ExecuteReport(QFileInfo reportFile)
{
	QString sha1sum = HashFile(reportFile.filePath());
	qDebug() << "Hash of file: " << sha1sum << "\n";

	QSettings settings("./replay-parser.ini", QSettings::IniFormat);
	bool alreadyReported = false;

	if (IsAlreadyReportedHash(sha1sum, &settings)){
		QMessageBox::StandardButton reply;
		reply = QMessageBox::question(this, "Report Loss", "It seems you have already reported this game. Are you sure you want to do it again?", QMessageBox::Yes|QMessageBox::No);
		if (reply == QMessageBox::No) 
			return;
		
		alreadyReported = true;
	}

	QString confEmail = settings.value("email").toString();
	QString confPwd = settings.value("token").toString();

	qDebug() << "Trying to report: " << reportFile.filePath() << "\n";


	QUrl url(baseUrl + "GameReport/ReportLoss");
	QNetworkRequest request(url);

	request.setRawHeader("Email", confEmail.toLocal8Bit());
	request.setRawHeader("Token", confPwd.toLocal8Bit());
	request.setRawHeader("ClientName", "console");

	QFile *file = new QFile(reportFile.filePath());
	file->open(QIODevice::ReadOnly);

	QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);
	QHttpPart replayPart;
	replayPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant(QString("form-data; name=\"ReplayLoss\"; filename=\"%1\"").arg(reportFile.fileName())));
	replayPart.setBodyDevice(file);
	file->setParent(multiPart); // we cannot delete the file now, so delete it with the multiPart

	multiPart->append(replayPart);

	QNetworkAccessManager *manager = new QNetworkAccessManager(this);

	//Properties are basically used as parameters when the SLOT is called
	manager->setProperty("alreadyReported", QVariant(alreadyReported));
	manager->setProperty("sha1sum", QVariant(sha1sum));
	multiPart->setParent(manager); // delete the multiPart with the reply
	
	connect(manager, SIGNAL(finished(QNetworkReply *)), this, SLOT(FinalizeExecuteReport(QNetworkReply *)));

	manager->post(request, multiPart);

	ShowLoader("Reporting game...");
}

void ReplayTool::FinalizeExecuteReport(QNetworkReply * reply)
{
	qDebug() << "Status code: " << reply->attribute(
        QNetworkRequest::HttpStatusCodeAttribute).toInt();
	
	QString responseString(reply->readAll());
	qDebug() << "Response: " << responseString;

	//Retrieve "parameters" from the sender object
	QString sha1sum = sender()->property("sha1sum").toString();
	bool alreadyReported = sender()->property("alreadyReported").toBool();

	loader->hide();

	if (reply->error() != 0)
	{
		QMessageBox messageBox;
		switch(reply->error()) {
			case 99: messageBox.critical(0, "Report loss", "Can't establish connection... Is your internet connection working?"); break;
			case 299: messageBox.critical(0, "Report loss", QString("Report refused... \n\n" + responseString + "\n\n" + reply->rawHeader("Date")) ); break;
			default: messageBox.critical(0, "Report loss", QString("Problem while logging in... \n") + reply->errorString()); break;
		}
		messageBox.setFixedSize(500,200);
		return;
	}

	else
	{
		QMessageBox messageBox;
		messageBox.information(0, "Report loss", QString("Successfully reported game!"));
		messageBox.setFixedSize(500,200);

		if (!alreadyReported)
		{	
			QSettings settings("./replay-parser.ini", QSettings::IniFormat);
			int size = settings.beginReadArray("AlreadyReportedHashes");
			settings.endArray();

			settings.beginWriteArray("AlreadyReportedHashes");

			settings.setArrayIndex(size);
			settings.setValue("checksum", sha1sum);

			settings.sync();
			settings.endArray();
			
		}
	}

	//Trigger a rescan of the folder
	if(!ReplayDirWatcher->directories().isEmpty())
	{
		DetectedModification(ReplayDirWatcher->directories().last() + "/leldummy.txt");
	}
}

bool ReplayTool::IsAlreadyReportedHash(QString sha1sum, QSettings * settings)
{
	// QVector<QVariant> retrievedList;
	QSet<QString> sha1sums;
	int size = settings->beginReadArray("AlreadyReportedHashes");
	
	for (int i = 0; i < size; ++i) {
		settings->setArrayIndex(i);
		sha1sums.insert(settings->value("checksum").toString());
	}
	settings->endArray();

	return sha1sums.contains(sha1sum);
}


void ReplayTool::UpdateReplayInfo(QFileInfo replayFinfo)
{
	if(!replayFinfo.exists()){
		MostRecentReplayLabel->setText(" -\n -");
		return;
	}
	
	MostRecentReplayLabel->setText(QString("") + replayFinfo.fileName() + "\n" + replayFinfo.lastModified().toString("dddd, MMMM d. hh:mm"));

	QString sha1sum = HashFile(replayFinfo.filePath());

	QSettings s("./replay-parser.ini", QSettings::IniFormat);
	if(IsAlreadyReportedHash(sha1sum, &s))
		MostRecentReplayLabel->setText(MostRecentReplayLabel->text() + "\nYou already reported this replay!");
	else 
		MostRecentReplayLabel->setText(MostRecentReplayLabel->text() + "\n ");
}

QString ReplayTool::HashFile(QString path)
{
	Release *rel = new Release();
	return rel->fileSHA1(path);
}

QFileInfo ReplayTool::FindMostRecentReplayFilePath(QString replayFolder)
{
	QDir dir(replayFolder);

	if (!dir.exists()) {
		qWarning("The directory does not exist");
		return QString("");
	}

	else
	{
		dir.setFilter(QDir::Files);
		dir.setSorting(QDir::Time);

		QFileInfoList list = dir.entryInfoList();

		foreach (QFileInfo finfo, list)
		{
			if(finfo.suffix().compare("bfme2replay", Qt::CaseInsensitive) == 0)
				return finfo;
		}
	}
	return QString("");
}

void ReplayTool::SetNewReplayFolder(QString selectedFolder)
{
	QString newPath = selectedFolder;
	ReplayDirBox->setText(selectedFolder);

	QFileInfo foundFile = FindMostRecentReplayFilePath(selectedFolder);

	QString pathBoxContent = newPath;

	//Not found?
	if(foundFile.fileName().compare("") == 0)
	{
		//Descend into folder/Replays and see if that one works
		QDir selectedFolderAsDir = QDir(selectedFolder);
		selectedFolderAsDir.cd("Replays/");

		newPath = selectedFolderAsDir.absolutePath();

		foundFile = FindMostRecentReplayFilePath(newPath);

		if(foundFile.fileName().compare("") == 0)
		{
			QMessageBox messageBox;
			messageBox.critical(0,"Log in", QString("No replays were found in the directory you selected!"));
			messageBox.setFixedSize(500,200);
		}
		else 
			pathBoxContent = newPath + "  " + checkmark;
	}
	else 
		pathBoxContent = newPath + "  " + checkmark;
	
	ReplayDirBox->setText(pathBoxContent);
	UpdateReplayInfo(foundFile);

	QSettings s("./replay-parser.ini", QSettings::IniFormat);
	s.setValue("path", newPath);
	s.sync();

	ReplayDirWatcher = new QFileSystemWatcher(this);
	ReplayDirWatcher->addPath(newPath);

	connect(ReplayDirWatcher, SIGNAL(directoryChanged(QString)), this, SLOT(DetectedDirModification(QString)));
	QStringList directoryList = ReplayDirWatcher->directories();
	Q_FOREACH(QString directory, directoryList)
		qDebug() << "Tracking directory " << directory <<"\n";
	
	RescanAllTrackingFiles(newPath);
}

void ReplayTool::ToggleAdvanced()
{
	advancedOptionsBox->show();
	QCheckBox *unlockAdvancedOptions = (QCheckBox*) sender();
	unlockAdvancedOptions->setDisabled(1);
}