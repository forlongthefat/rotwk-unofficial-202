/*
 * LauncherWindow.cpp
 *
 * Copyright 2017 Bram Bosch <bagbosch@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "LauncherWindow.h"


#include <QDebug>
#include <QTimer>

LauncherWindow::LauncherWindow(int argc)
{
	QWidget *widget = new QWidget;
	setCentralWidget(widget);

	QPixmap bkgnd("./bg.png");
	/// bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
	QPalette palette;
	palette.setBrush(QPalette::Background, bkgnd);
	this->setPalette(palette);


	// Create menus
	QMenu *menu = new QMenu("Tools");
	QMenu *menu2 = new QMenu("Help");
	QAction *resfix = new QAction("Resolution fixer", this);
	QAction *reptool = new QAction("League report tool", this);
	QAction *about = new QAction("About", this);
	QAction *tech = new QAction("Tech Support", this);
	menu->addAction(resfix);
	menu->addAction(reptool);
	menu2->addAction(tech);
	menu2->addAction(about);
	menuBar()->addMenu(menu);
	menuBar()->addMenu(menu2);


	// Labels
	QFont labelFont;
	labelFont.setBold(true);
	labelFont.setPointSize(10);
	QString LabelStyle = QString("QLabel { color : rgb(10,30,30); }");

	QLabel *cboxLabel = new QLabel("Main:", this);
	QLabel *cbox2Label = new QLabel("\n\n\nOld 2.02 versions:", this);
	QLabel *cbox3Label = new QLabel("\nMusic modules:", this);
	QLabel *cbox4Label = new QLabel("\nMiscellaneous:", this);
	QLabel *emptyLabel = new QLabel("\n", this);
	cboxLabel->setStyleSheet(LabelStyle);
	cboxLabel->setFont(labelFont);
	cbox2Label->setStyleSheet(LabelStyle);
	cbox2Label->setFont(labelFont);
	cbox3Label->setStyleSheet(LabelStyle);
	cbox3Label->setFont(labelFont);
	cbox4Label->setStyleSheet(LabelStyle);
	cbox4Label->setFont(labelFont);


	// Push buttons
	applyBtn = new QPushButton("apply", this);
	apply2Btn = new QPushButton("apply", this);
	apply3Btn = new QPushButton("apply", this);
	apply4Btn = new QPushButton("apply", this);
	QPushButton *verifyBtn = new QPushButton("Scan all modules" , this);


	// Dropdown menus
	ConfigParser p("./launcher_releases");

	cbox = new QComboBox(this);
	QVector<QString> result = p.getReleaseNames(1);
	foreach (QString releaseName, result)
	{
		cbox->addItem(releaseName);
	}

	cbox2 = new QComboBox(this);
	result = p.getReleaseNames(2);
	foreach (QString releaseName, result)
	{
		cbox2->addItem(releaseName);
	}

	cbox3 = new QComboBox(this);
	result = p.getReleaseNames(3);
	foreach (QString releaseName, result)
	{
		cbox3->addItem(releaseName);
	}

	cbox4 = new QComboBox(this);
	result = p.getReleaseNames(4);
	foreach (QString releaseName, result)
	{
		cbox4->addItem(releaseName);
	}


	// Geometry of the buttons/dropdowns
	const int dropdownWidth = 200;
	const int buttonWidth = 60;
	const int verifyButtonWidth = 130;

	cbox->setMinimumWidth(dropdownWidth);
	cbox2->setMinimumWidth(dropdownWidth);
	cbox3->setMinimumWidth(dropdownWidth);
	cbox4->setMinimumWidth(dropdownWidth);
	applyBtn->setMaximumWidth(buttonWidth);
	apply2Btn->setMaximumWidth(buttonWidth);
	apply3Btn->setMaximumWidth(buttonWidth);
	apply4Btn->setMaximumWidth(buttonWidth);
	verifyBtn->setMinimumWidth(verifyButtonWidth);
	verifyBtn->setMaximumWidth(verifyButtonWidth);


	// Window lay-out
	QGridLayout *grid = new QGridLayout;
	grid->setAlignment(applyBtn, Qt::AlignHCenter);
	grid->setAlignment(verifyBtn, Qt::AlignHCenter);

	grid->addWidget(cboxLabel, 0, 0);
	grid->addWidget(cbox, 1, 0);			grid->addWidget(applyBtn, 1, 1, Qt::AlignLeft);

	grid->addWidget(cbox2Label, 2, 0);
	grid->addWidget(cbox2, 3, 0);			grid->addWidget(apply2Btn, 3, 1, Qt::AlignLeft);

	grid->addWidget(cbox3Label, 4, 0);
	grid->addWidget(cbox3, 5, 0);			grid->addWidget(apply3Btn, 5, 1, Qt::AlignLeft);

	grid->addWidget(cbox4Label, 6, 0);
	grid->addWidget(cbox4, 7, 0);			grid->addWidget(apply4Btn, 7, 1, Qt::AlignLeft);

	grid->addWidget(emptyLabel, 8, 0, Qt::AlignCenter);
	grid->addWidget(verifyBtn, 9, 0, 1, 0, Qt::AlignCenter);

	widget->setLayout(grid);


	// Connect signals
	connect(applyBtn, SIGNAL(clicked()), this, SLOT(ApplyRelease()));
	connect(apply2Btn, SIGNAL(clicked()), this, SLOT(ApplyRelease()));
	connect(apply3Btn, SIGNAL(clicked()), this, SLOT(ApplyRelease()));
	connect(apply4Btn, SIGNAL(clicked()), this, SLOT(ApplyRelease()));
	connect(verifyBtn, SIGNAL(clicked()), this, SLOT(VerifyRelease()));
	connect(resfix, SIGNAL(triggered()), this, SLOT(LaunchResfix()), Qt::UniqueConnection);
	connect(reptool, SIGNAL(triggered()), this, SLOT(LaunchReptool()), Qt::UniqueConnection);
	connect(about, SIGNAL(triggered()), this, SLOT(OpenAboutPage()), Qt::UniqueConnection);
	// connect(about, SIGNAL(triggered()), this, SLOT(OpenAboutPage()), Qt::UniqueConnection);
	connect(tech, SIGNAL(triggered()), this, SLOT(OpenTechSupportPage()), Qt::UniqueConnection);


	//Instantly skip to replay parser mode
	if(argc > 1)
		LaunchReptool();
}

void LauncherWindow::LaunchResfix()
{
	// Set up the pop-up window for the resolution fixer
	ResToolPopup = new ResTool();
	ResToolPopup->setWindowTitle("RotWK resolution fixer");
	ResToolPopup->resize(1, 1); //set to smallest possible size
	ResToolPopup->setMaximumWidth(1); ResToolPopup->setMaximumHeight(1); //lock to that size
	ResToolPopup->setGeometry(
		QStyle::alignedRect(
			Qt::LeftToRight,
			Qt::AlignCenter,
			QSize(870, 470),  // this should be an approximation of how large the window actually is
			qApp->desktop()->availableGeometry()
		)
	);
	ResToolPopup->show();
}

void LauncherWindow::LaunchReptool()
{
	qDebug() << __LINE__;
	// Set up the pop-up window for the resolution fixer
	RepToolPopup = new ReplayTool();
	RepToolPopup->setWindowTitle("RotWK League Replay Reporter");
	RepToolPopup->resize(1, 1); //set to smallest possible size
	RepToolPopup->setMaximumWidth(1); RepToolPopup->setMaximumHeight(1); //lock to that size
	RepToolPopup->setGeometry(
		QStyle::alignedRect(
			Qt::LeftToRight,
			Qt::AlignCenter,
			QSize(800, 200),  // this should be an approximation of how large the window actually is
			qApp->desktop()->availableGeometry()
		)
	);

	RepToolPopup->ShowLoader("Loading the RotWK Replay Reporter...");

	QTimer *timer = new QTimer(this);
	timer->setSingleShot(true);
    connect(timer, SIGNAL(timeout()), RepToolPopup, SLOT(SetupWindow()));
    timer->start(1000);

	// this->hide();
	RepToolPopup->show();
}

void LauncherWindow::ApplyRelease()
{
	QObject* obj = sender();
	QComboBox *cBoxToUse;

	if( obj == applyBtn )
		cBoxToUse = cbox;
	else if( obj == apply2Btn )
		cBoxToUse = cbox2;
	else if( obj == apply3Btn )
		cBoxToUse = cbox3;
	else if( obj == apply4Btn )
		cBoxToUse = cbox4;
	else
		cBoxToUse = cbox;

	ConfigParser p("./launcher_releases");
	Release wejoo = p.createRelease(cBoxToUse->currentText());
	QString ok = wejoo.applyRelease();

	if (ok.compare("") == 0) {
		QMessageBox messageBox;
		messageBox.information(0,"Success", QString("Applied module ") + cBoxToUse->currentText());
		messageBox.setFixedSize(500,200);
	}
	else {
		QMessageBox messageBox;
		messageBox.critical(0,"Fail", QString("Error: ") + ok);
		messageBox.setFixedSize(500,200);
	}
}


void LauncherWindow::VerifyRelease()
{
	QMessageBox::StandardButton reply;
	reply = QMessageBox::question(this, "Confirm", "This will scan all modules and evaluate whether they are enabled or not. It may take around half a minute. \n\nDo you wish to continue?", QMessageBox::Yes|QMessageBox::No);

	if (!(reply == QMessageBox::Yes)) {
		return;
	}

	FILE * logF =  fopen("launcher202_log.txt", "w");
	QTextStream logfile(logF, QIODevice::WriteOnly);

	QString outputText = "Modules currently enabled: \n";

	ConfigParser p("./launcher_releases");
	QVector<QString> result = p.getReleaseNames(1) + p.getReleaseNames(2) + p.getReleaseNames(3) + p.getReleaseNames(4);

	foreach (QString currentRelease, result)
	{
		Release wejoo = p.createRelease(currentRelease);
		QString ok = wejoo.integrityCheckRelease();

		if (ok.compare("") == 0) {
			logfile << currentRelease << " enabled. \n";
			outputText.append(QString("- ").append(currentRelease.append(QString("\n"))));
		}
		else
			logfile << currentRelease << " Says: " << ok << "\n";
	}

	QMessageBox messageBox;
	messageBox.information(0, "Report", outputText);

	logfile.flush();
	fclose(logF);
}

void LauncherWindow::OpenAboutPage()
{
	QMessageBox messageBox;
	messageBox.setWindowTitle("About");
	messageBox.setText("Code by Brabox\n\nSpecial thanks to www.gamereplays.org\n\n\nLicense: LGPL\nSource code should be packed with this executable. If not, try here: https://gitlab.com/forlongthefat/rotwk-unofficial-202 \nor contact me (bag.bosch@gmail.com).");

	messageBox.exec();
}

void LauncherWindow::OpenTechSupportPage()
{
	QMessageBox messageBox;
	messageBox.setWindowTitle("Tech Support");
	messageBox.setText("The 2.02 community offers tech support via www.gamereplays.org. \n\nThe tech-support section can be found here: \n\nhttps://www.gamereplays.org/community/index.php?showforum=3709");

	messageBox.exec();
}
