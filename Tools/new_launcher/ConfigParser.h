/*
 * ConfigParser.h
 *
 * Copyright 2017 Bram Bosch <bagbosch@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#pragma once

// For console output
#include <QTextStream>

// For file handling
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QVector>
#include <QSettings>

#include "SwitchFile.h"
#include "Release.h"

#define ALL_CATEGORIES -1
#define DUMMY_FILE "dummy.202"

class ConfigParser
{
	public:
		ConfigParser();
		ConfigParser(QString dir);
		QVector<QString> getReleaseNames(int category);
		Release createRelease(QString releaseName);

	private:
		//Attributes
		QString directory;

};
