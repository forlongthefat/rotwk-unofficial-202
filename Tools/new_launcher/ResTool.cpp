/*
 * ResTool.cpp
 *
 * Copyright 2017 Bram Bosch <bagbosch@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include "ResTool.h"
#include "UserDirFind.h"

#include <QGridLayout>
#include <QFileDialog>
#include <QProcess>
#include <QDebug>
#include <QDataStream>


ResTool::ResTool(QWidget *parent) : QWidget(parent)
{
	QString guessedXreso = QString("%1").arg(qApp->desktop()->geometry().width());
	QString guessedYreso = QString("%1").arg(qApp->desktop()->geometry().height());

	QLabel *Xlabel = new QLabel("Width:", this);
	QLabel *Ylabel = new QLabel("Height:", this);

	XresBox = new QLineEdit(guessedXreso, this);
	YresBox = new QLineEdit(guessedYreso, this);

	QPushButton *applyBtn = new QPushButton("Apply", this);


	QGridLayout *grid = new QGridLayout(this);
	grid->addWidget(Xlabel, 0, 0);
	grid->addWidget(Ylabel, 0, 1);

	grid->addWidget(XresBox, 1, 0);
	grid->addWidget(YresBox, 1, 1);

	grid->addWidget(applyBtn, 2, 0, 2, 0);

	setLayout(grid);

	connect(applyBtn, SIGNAL(clicked()), this, SLOT(ApplyResolutionToFile()));
}

void ResTool::ApplyResolutionToFile()
{
	QString xres = XresBox->displayText();
	QString yres = YresBox->displayText();
	bool xOK, yOK;

	int xr = xres.toInt(&xOK, 10);
	int yr = yres.toInt(&yOK, 10);

	if(!xOK || !yOK)
	{
		QMessageBox resfailBox;
		resfailBox.critical(0,"Error", QString("The resolution you chose contains non-numerical characters."));
		resfailBox.setFixedSize(500,200);
		return;
	}

	// I don't simply concatenate the strings, but instead the actual numbers obtained from them.
	// This ensures we're dealing with numbers and not with material that possibly translates to a valid number.
	// E.g. what if an input "1e3" is interpreted as 1000 by toInt? I'd rather work with 1000 then.
	QString combined = QString("%1 %2").arg(xr).arg(yr);
	
	QString dirs[2];
	UserDirFind::FindRotwkUserDirs(dirs);

	QString optionsFile(dirs[0] + "/Options.ini");
	QString optionsFileB(dirs[1] + "/Options.ini");
	QFile opts(optionsFile);
	QFile optsB(optionsFileB);

	// It doesn't exist!?
	if(!(opts.exists()) && !(optsB.exists()))
	{
		QMessageBox messageBox;
		messageBox.critical(0,"Error", QString("Can't locate the options.ini file. I looked for it here: \n\n- " + optionsFile + "\n\n- " + optionsFileB));
		messageBox.setFixedSize(500,200);
		return;
	}

	if(optsB.exists())
		optionsFile = optionsFileB;

	QSettings s(optionsFile, QSettings::IniFormat);
	s.setValue("Resolution", combined);
	s.sync();

	QMessageBox messageBox;
	messageBox.information(0, "Resolution fixer", "New resolution <" + combined + "> was set.\n\nTry launching the game now.");

	return;
}


