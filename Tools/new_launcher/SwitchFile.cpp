/*
 * SwitchFile.cpp
 *
 * Copyright 2017 Bram Bosch <bagbosch@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "SwitchFile.h"

#include <QDebug>
#include <QTextStream>

SwitchFile::SwitchFile()
{
	fileName = "Error:NoFile";
	checksum = "";
	maxFileSize = 0;
}

SwitchFile::SwitchFile(QString newfileName, QString newchecksum, int newmaxFileSize)
{
	fileName = newfileName;
	checksum = newchecksum;
	maxFileSize = newmaxFileSize;
}

QString SwitchFile::getFileName()
{
	return fileName;
}

QString SwitchFile::getChecksum()
{
	return checksum;
}

int SwitchFile::getMaxFileSize()
{
	return maxFileSize;
}

QString SwitchFile::performSwitch()
{
	if(isAlreadyActive())
		return "";

	QString foundFile = findSmallestChecksumMatch(checksum, maxFileSize);

	if (foundFile.compare("") == 0)
		return QString("Can not find any file with checksum ") + checksum;

	QString success = SwitchFiles(fileName, foundFile);

	return success;
}

bool SwitchFile::isAlreadyActive()
{
	if(checksum.compare(fileSHA1(fileName)) == 0)
		return true;
	else
		return false;
}

QString SwitchFile::findSmallestChecksumMatch(QString checksum, int maxFileSizeMB)
{
	QDir dir(".");

	if (!dir.exists()) {
		qWarning("The directory does not exist");
		return "";
	}

	else
	{
		dir.setFilter(QDir::Files);
		dir.setSorting(QDir::Size | QDir::Reversed);

		QFileInfoList list = dir.entryInfoList();

		foreach (QFileInfo finfo, list)
		{
			if ( finfo.size() <= (maxFileSizeMB  << 20) )  // multiply by  2^20 = 2^10 * 2^10 = 1024 * 1024 -> bytes -> KB -> MB
			{
				QString name = finfo.fileName();
				QString current_checksum = fileSHA1(name);

				if(current_checksum.compare(checksum) == 0)
					return name;
			}
		}
	}
	return "";
}

/**
 * Switches origfile and newfile.
 *
 * NOTE: If origfile does not exist, newfile is still moved to origfile's specified location.
 *       = treated as success -> no warning
 *
 *
 * @return true if successful (or if files are identical), false if not
 *      fail cases:
 *        - unable to move origfile anywhere (permissions?)
 *        - absence of newfile
 */
QString SwitchFile::SwitchFiles(QString origfile, QString newfile)
{
	if (origfile.compare(newfile) == 0)  // two names match? no work to do.
		return "";

	QFile newFile(newfile);
	if(!(newFile.exists())) // no newfile? don't bother then
		return QString("File ") + newfile + " does not exist.";

	QFile currDat(origfile);
	QString number = "nowhere because it didn't exist!";

	// Perhaps there is no old game.dat. If so, we don't need to move it.
	if (currDat.exists())
	{
		int i = 0;

		// Keep trying to move game.dat to game<number>.off until we succeed
		// (this is a temporary location)
		while(i != -1)
		{
			i++;

			// we've tried 2000 numbers, apparently sth else is wrong.
			if(i > 2000)
				return "Cannot move the file " + origfile + " anywhere...";

			number = QString(origfile).append(QString("%1.off").arg(i));

			bool moveOK = currDat.rename(number);

			// we moved succesfully! let's abort the loop then.
			if(moveOK)
				i = -1;
		}
	}

	// this should never fail because we just moved 'origfile' out of the way
	newFile.rename(origfile);

	// rename that ugly temporary file-name back to 'newfile' so it's really a switch action
	QFile moveBack(number);
	moveBack.rename(newfile);

	return "";
}

/**
 * Calculates an sha1 checksum from a file.
 *
 * Will return the string "Error" if something went wrong
 *
 * Mapping:  QString -> QString
 */
QString SwitchFile::fileSHA1(QString file_name)
{
	const char * output = "Error";

	QFile f(file_name);
	QCryptographicHash hash(QCryptographicHash::Sha1);

	if (f.open(QFile::ReadOnly))
	{
		hash.addData(f.readAll());

		output = hash.result().toHex();
	}

	return QString(output);
}


