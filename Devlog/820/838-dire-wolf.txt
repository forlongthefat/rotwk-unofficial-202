- Dire Wolf:
--- Pikemen scalar decreased to 180% (from 325%);
--- Upgraded pierce armour increased to 60% (from 70%); un-upgraded pierce armour increased to 90% (from 120%);
--- Upgraded cavalry armour decreased to 90% (from 80%); un-upgraded cavalry armour increased to 120% (from 140%);
--- Upgraded slash armour decreased to 100% (from 80%);
--- Health increased to 275 (from 250).
