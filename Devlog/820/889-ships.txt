- Bombard Ship range decreased to 500 (from 600); Bombard Ship accuracy improved significantly (should match that of regular siege).
- All ships now benefit from stances with the following attributes:
--- Aggressive Stance: increases attack range by 10% and shroud clearing range by 15%; reduces armour by 15%;
--- Hold Ground Stance: increases armour by 10%; reduces attack range by 10% and shroud clearing range by 5%.
- Transport Ship health increased to 600 (from 300).
- Doom and Storm Ship positions in Shipwright swapped with Bombard Ships.
