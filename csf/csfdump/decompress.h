#ifndef H__DECOMPRESS
#define H__DECOMPRESS

#ifdef __cplusplus
extern "C" {
#endif

unsigned int swapbytes(const unsigned char * buffer, unsigned int count);
int GetUncompressedSize(const unsigned char * buffer);
void DecompressData(unsigned char * inbuffer, unsigned char * outbuffer);

#ifdef __cplusplus
}
#endif

#endif
